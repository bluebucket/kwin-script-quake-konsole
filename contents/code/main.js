/*****************************************************************************
*   kwin-script-quake-konsole - Make Konsole behave like Quake console       *
*   Copyright (C) 2023  Dave Buchan                                          *
*                                                                            *
*   This program is free software: you can redistribute it and/or modify     *
*   it under the terms of the GNU General Public License as published by     *
*   the Free Software Foundation, either version 3 of the License, or        *
*   (at your option) any later version.                                      *
*                                                                            *
*   This program is distributed in the hope that it will be useful,          *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*   GNU General Public License for more details.                             *
*                                                                            *
*   You should have received a copy of the GNU General Public License        *
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*****************************************************************************/

function runScript() {
	const clients = workspace.clientList();
	//print("Got clientList");

	for (var i = 0; i < clients.length; i++) {
		if (clients[i].caption.indexOf('— Konsole') > 1) {
			//print("Found Konsole");
			if (clients[i].active) {
				clients[i].minimized = true;
			}
			else {
				clients[i].frameGeometry = {
					x: 1334,
					y: 1108,
					height: 886,
					width: 1226,
				}

				clients[i].minimized = false;
				workspace.activeClient = clients[i];
			}
			break;
		}
	}
}

registerShortcut("Quake Konsole", "Quake Konsole", "Launch (7)", function () {
	runScript();
});
