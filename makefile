clean:
	@echo "Cleaning..."
	@rm -f kwin-script-quake-konsole.kwinscript

build: clean
	@echo "Building..."
	@zip kwin-script-quake-konsole.kwinscript -r contents LICENSE metadata.json

uninstall:
	@echo "Uninstalling..."
	@-plasmapkg2 -t kwinscript -r kwin-script-quake-konsole

install-pkg: uninstall build
	@echo "Installing..."
	@plasmapkg2 -t kwinscript -i kwin-script-quake-konsole.kwinscript

install:
	@echo "Installing..."
	@kpackagetool5 --type=KWin/Script -i . 
