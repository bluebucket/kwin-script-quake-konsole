# Quake Konsole

A Kwin script that minimizes and restores Konsole, from a keyboard shortcut, faking a the Quake console.

### Setup

Just clone the repo. `make` commands have been set up to do all the things.

* `make build` - Build the `.kwinscript` file
* `make clean` - Remove the `.kwinscript` file
* `make install` - Install the script to your Plasma
* `make uninstall` - Uninstall the script from your Plasma
